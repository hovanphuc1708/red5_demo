<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.net.Inet4Address,java.net.URLConnection"%>
<%@ page import="com.red5pro.server.secondscreen.net.NetworkUtil"%>

<%@ page import="java.io.*,java.util.Map,java.util.ArrayList,java.util.regex.*,java.net.URL,java.nio.charset.Charset"%>
<%@ page import="org.springframework.context.ApplicationContext,
          org.springframework.web.context.WebApplicationContext,
          com.red5pro.example.ExampleApplication,
          java.util.List, java.util.ArrayList,
          java.net.Inet4Address"%>
<% 
ApplicationContext appCtx = (ApplicationContext) application.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
ExampleApplication service = (ExampleApplication)appCtx.getBean("web.handler");
List<String> streamNames = service.getLiveStreams();
String localIp = NetworkUtil.getLocalIpAddress();
StringBuffer buffer = new StringBuffer();
if(streamNames.size() == 0) {
  buffer.append("No streams found. Refresh if needed.");
}else {
  String classUl = "list-group";
  String classLi = "list-group-item";
 
  buffer.append("<ul class="+classUl+">\n");
   String action = "list-group-item-action"+""+"active";
   String value = request.getParameter("name");

    for (String streamName : streamNames) {
    String classA = streamName.equals(value) ? action : "list-group-item-action";    
    String url = "http://"+localIp+":5080/example/subscriber.jsp?name=" + streamName;
    buffer.append("<li class="+classLi+" ><a href="+ url +" class="+classA+"> StreamName: "+streamName+ "</a></li>");
    }
    buffer.append("</ul>\n");
  }
%>

<!doctype html>

<html>
  <head> 
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <style>
      a {
        text-decoration: none;
      }
    </style>
  </head>
  <body>
    <div>
      # Streams on Red5ProLive
      <p id="hostName"><%=localIp%></p>
      <div class="container">
        <div class="row">
          <div class="col-12 pb-4">
            <button type="button" class="btn btn-primary btn-sm" onclick="window.location.reload()">Refesh</button>
          </div>
          <div class="col-3" class="buffer">
            <%=buffer.toString()%>
          </div>
          <div class="col-9">
            <video
            id="red5pro-subscriber"
            with="400"
            height="300"
            autoplay
            controls
            playsinline
          ></video>
          </div>
         
        </div>
      </div>
   
    </div>
    <div id="video-container">
      <!-- <input type="text" id="stream" placeholder="Input room name" /> -->
      <!-- <button type="button" onclick="start(window.red5prosdk)">Join</button> -->
      <button type="button" onclick="pause()">Turn Off Video</button>
      <button type="button" onclick="play()">Turn On Video</button>
      <button type="button" onclick="outStream(window)">Out</button>
      
    </div>
  
  </body>

  <!-- WebRTC Shim -->
  <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
  <!-- Exposes `red5prosdk` on the window global. -->
  <script src="https://unpkg.com/red5pro-webrtc-sdk@latest/red5pro-sdk.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>

  <script>
    // get dom element
    // const inputValue = document.getElementById("stream");
    const video = document.getElementById("red5pro-subscriber");
    const hostName = document.getElementById("hostName");
    const buffer = document.querySelector(".buffer");
    const listStream = ['123','321']

    // config web socket
    const config = {
      protocol: "ws",
      port: 5080,
      host: "localhost",
      app: "example",
      streamName: "",
      muteOnAutoRestriction: false,
      rtcConfiguration: {
        iceServers: [{ urls: "stun:stun2.l.google.com:19302" }],
        iceCandidatePoolSize: 2,
        bundlePolicy: "max-bundle",
      }, // See https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/RTCPeerConnection#RTCConfiguration_dictionary
        mediaElementId: "red5pro-subscriber",
        subscriptionId:
          "" + Math.floor(Math.random() * 0x10000).toString(16),
    };

    const subscriber = new red5prosdk.RTCSubscriber();

    const streamName = location.search.split('name=').splice(1).join('').split('&')[0];
    console.log(streamName);

    // init web socket subscribe channel
     function start(red5prosdk) {

      try {
          subscriber.init({
          ...config,
          host: hostName.innerText||location.hostname,
          ...(streamName? {streamName:streamName}: {})
        });
        subscriber.subscribe();
      } catch (e) {
        console.log(e);
        // An error occured in establishing a subscriber session.
      }
    };

    // check stream tồn tại trong mảng list stream
if(!listStream.includes(streamName)) {

    // console.log(33333);
 
}

    if(streamName) {
      start(),
      console.log(location.hostname);
    }
    //pause video for subscriber
    function pause() {
      subscriber
        .pause()
        // .then(() => console.log("Stopped publisher stream!"))
        .then(() => console.log("Stopped pause stream!"))
        .catch(() => console.log("Failed to pause stream!"));
    }
    
    //Out liveStream
    function outStream(window) {

      const urlOrigin =  window.location.origin + "" +window.location.pathname
      subscriber.unsubscribe()
        .then(history.pushState({}, null,urlOrigin))
        .then(() => console.log("Out strem!"))
        .catch(() => console.log("Failed to pause stream!"));
      
    }
    function play() {
      subscriber
        .play()
        .then(() => console.log("Playing stream!"))
        .catch(() => console.log("Failed to pause stream!"));
    }
  </script>
</html>
