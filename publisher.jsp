<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.net.Inet4Address,java.net.URLConnection"%>
<%@ page import="com.red5pro.server.secondscreen.net.NetworkUtil"%>

<%@ page import="java.io.*,java.util.Map,java.util.ArrayList,java.util.regex.*,java.net.URL,java.nio.charset.Charset"%>
<%@ page import="org.springframework.context.ApplicationContext,
          org.springframework.web.context.WebApplicationContext,
          com.red5pro.example.ExampleApplication,
          java.util.List, java.util.ArrayList,
          java.net.Inet4Address"%>
<% 
ApplicationContext appCtx = (ApplicationContext) application.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
ExampleApplication service = (ExampleApplication)appCtx.getBean("web.handler");
List<String> streamNames = service.getLiveStreams();
String localIp = NetworkUtil.getLocalIpAddress();
StringBuffer buffer = new StringBuffer();
if(streamNames.size() == 0) {
  buffer.append("No streams found. Refresh if needed.");
}else {
  buffer.append("<ul>\n");
    for (String streamName : streamNames) {
    String url = "http://"+localIp+":5080/example/subscriber.jsp?name=" + streamName;
    buffer.append("<li><a href="+ url +"> StreamName: "+streamName+ "</a></li>");
    }
    buffer.append("</ul>\n");
  }
%>

<html>
  <head> 
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
  </head>
  <body>
    <div>
      <div class="container">
          <div>
            # Streams on Red5ProLive
            <p id="hostName"><%=localIp%></p>
          </div>
          <!-- Input stream name -->
          <div class="col-12 pb-4">
            <input type="text" id="stream" placeholder="Input room name" />
            <button type="button" onclick="start(window.red5prosdk)">Start Stream</button>
            <!-- <button type="button" onclick="start(window.red5prosdk)">start</button> -->
            <button type="button" onclick="stop()">Off Stream</button>
            <button type="button" onclick="muted()">muteVideo</button>
            <button type="button" onclick="unMuted()">unMuteVideo</button>
          </div>
          <div class="row">
            <!-- Event log -->
            <div class="col-5">
              <h4 style="color: blue;margin-top: 20px;">Event log:</h4>
              <button onclick="clearLog()" >Clear</button>
    
              <div id="event-log"  style="color: blue;margin-top: 20px;">  
                  <!-- div clear log -->
              </div>
            </div>
            <!-- Publisher video -->
            <div class="col-7">
              <div id="statistics-field" class="statistics-field"> </div>
                <video
                  id="red5pro-publisher"
                  with="400"
                  height="300"
                  muted
                  autoplay
                  controls
                ></video>
            </div>
          </div>
      </div>
   
    </div>
  </body>

  <!-- WebRTC Shim -->
  <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
  <!-- Exposes `red5prosdk` on the window global. -->
  <!-- <script src="https://unpkg.com/red5pro-webrtc-sdk@latest/red5pro-sdk.min.js"></script> -->
  <script src="https://cdn.jsdelivr.net/npm/red5pro-webrtc-sdk"></script>

  <script>
      var bitrateInterval = 0;
  var vRegex = /VideoStream/;
  var aRegex = /AudioStream/;

  // Based on https://github.com/webrtc/samples/blob/gh-pages/src/content/peerconnection/bandwidth/js/main.js
  window.trackBitrate = function (connection, cb, resolutionCb) {
    //Check connection
    console.log("connect", connection);
    window.untrackBitrate(cb);
    //    var lastResult;
    var lastOutboundVideoResult;
    var lastInboundVideoResult;
    var lastInboundAudioResult;
    bitrateInterval = setInterval(function () {
      connection.getStats(null).then(function(res) {
        res.forEach(function(report) {
          var bytes;
          var packets;
          var now = report.timestamp;
          if ((report.type === 'outboundrtp') ||
              (report.type === 'outbound-rtp') ||
              (report.type === 'ssrc' && report.bytesSent)) {
            bytes = report.bytesSent;
            packets = report.packetsSent;
            if ((report.mediaType === 'video' || report.id.match(vRegex))) {
              if (lastOutboundVideoResult && lastOutboundVideoResult.get(report.id)) {
                // calculate bitrate
                var bitrate = 8 * (bytes - lastOutboundVideoResult.get(report.id).bytesSent) /
                    (now - lastOutboundVideoResult.get(report.id).timestamp);

                cb(bitrate, packets);

              }
              lastOutboundVideoResult = res;
            }
          }
          // playback.
          else if ((report.type === 'inboundrtp') ||
              (report.type === 'inbound-rtp') ||
              (report.type === 'ssrc' && report.bytesReceived)) {
            bytes = report.bytesReceived;
            packets = report.packetsReceived;
            if ((report.mediaType === 'video' || report.id.match(vRegex))) {
              if (lastInboundVideoResult && lastInboundVideoResult.get(report.id)) {
                // calculate bitrate
                bitrate = 8 * (bytes - lastInboundVideoResult.get(report.id).bytesReceived) /
                  (now - lastInboundVideoResult.get(report.id).timestamp);

                cb('video', report, bitrate, packets - lastInboundVideoResult.get(report.id).packetsReceived);
              }
              lastInboundVideoResult = res;
            }
            else if ((report.mediaType === 'audio' || report.id.match(aRegex))) {
              if (lastInboundAudioResult && lastInboundAudioResult.get(report.id)) {
                // calculate bitrate
                bitrate = 8 * (bytes - lastInboundAudioResult.get(report.id).bytesReceived) /
                  (now - lastInboundAudioResult.get(report.id).timestamp);

                cb('audio', report, bitrate, packets - lastInboundAudioResult.get(report.id).packetsReceived);
              }
              lastInboundAudioResult = res;
            }
          }
          else if (resolutionCb && report.type === 'track') {
            var fw = 0;
            var fh = 0;
            if (report.kind === 'video' ||
                (report.frameWidth || report.frameHeight)) {
              fw = report.frameWidth;
              fh = report.frameHeight;
              if (fw > 0 || fh > 0) {
                resolutionCb(fw, fh);
              }
            }
          }
        });
      });
    }, 1000);
  }

  window.untrackBitrate = function() {
    clearInterval(bitrateInterval);
  }

  </script>

  <script>
    // get dom element
    const inputValue = document.getElementById("stream");

    // khởi tạo instance
    const rtcPublisher = new red5prosdk.RTCPublisher();
    const rtcSubscriber = new red5prosdk.RTCSubscriber();
    const publisher = new red5prosdk.Red5ProPublisher();
    //get hostName
    const hostName = document.getElementById("hostName");
    //Khởi tạo bitrate doom
    const statisticsField = document.getElementById('statistics-field');

  
    // config socket
    const config = {
      protocol: "ws",
      host: "localhost",
      port: 5080,
      app: "example",
      streamName: "mystream",
      rtcConfiguration: {
        iceServers: [{ urls: "stun:stun2.l.google.com:19302" }],
        iceCandidatePoolSize: 2,
        bundlePolicy: "max-bundle",
      }, // See https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/RTCPeerConnection#RTCConfiguration_dictionary
    };


  //On getUserMedia

    // hàm subscribe
    async function subscribe() {
      try {
        await rtcSubscriber.init(config);
        await rtcSubscriber.play(); // dùng hàm play chạy video
        rtcPublisher.on('*', onPublisherEvent);
        //Get Bitrate and Packets sent
        await window.trackBitrate(rtcPublisher.getPeerConnection(), onBitrateUpdate);
        console.log("Playing!");
      } catch (error) {
        console.log("Could not play: " + error);
      }
    }
    
    //And update Bitrate
    function onBitrateUpdate (bitrate, packets) {
    statisticsField.classList.remove('hidden');
    statisticsField.innerText = 'Bitrate: ' + (bitrate === 0 ? 'N/A' : Math.floor(bitrate)) + '.   Packets Sent: ' + packets + '.';
  }

    // start web socket red5pro
    async function start(red5prosdk) {
      try {
        await rtcPublisher.init({
          ...config,
          streamName: inputValue.value,
          host: hostName.innerText||location.hostname,
        });
        rtcPublisher.on('*', onPublisherEvent);
        await rtcPublisher.on(
          red5prosdk.PublisherEventTypes.PUBLISH_START,
          subscribe
        );
        
        console.log("Publishing!");
        rtcPublisher.publish();
  

       
      } catch (error) {
        console.error("Could not publish: " + error);
      }
    }
    
    

    // get event log
    function onPublisherEvent (event) {
      var eventLog = '[Red5ProPublisher] ' + event.type + '.';

      document.getElementById("event-log").innerHTML += "<p> "+eventLog+" </p>";
      console.log(eventLog);

    }

    //stop web socket red5 pro
    async function stop() {
      try {
        //Stop connect web socket
        await rtcPublisher.unpublish();
        //
        await rtcPublisher.unpreview();
        rtcPublisher.off('*', onPublisherEvent);
        console.log("Stop live stream!!!");
      } catch (error) {
        console.error(error);
      }
    }
    //muteVideo
    async function muted() {
      try {
        await rtcPublisher.muteVideo();
        console.log("muteVideo!!!");
      } catch (error) {
        console.error(error);
      }
    }
    //unMuted
    async function unMuted() {
      try {
        await rtcPublisher.unmuteVideo();
        console.log("unMuteVideo!!!");
      } catch (error) {
        console.error(error);
      }
    }

    //Clear evenLog
    //clear log
   function clearLog(){
    document.getElementById("event-log").innerHTML =""
   }

  </script>
</html>