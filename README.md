# RED5_DEMO



## Getting started
## ExampleApplication method
@Override
	public boolean appStart(IScope app) {
		log.info("AppStart");
		return super.appStart(app);
	}

	@Override
	public boolean appConnect(IConnection conn, Object[] params) {
		log.info("AppConnect");
		return super.appConnect(conn, params);
	}

	@Override
	public void appDisconnect(IConnection conn) {
		log.info("AppDisConnect");
		super.appDisconnect(conn);
	}

	public void streamBroadcastStart(IBroadcastStream stream) {

        IConnection connection = Red5.getConnectionLocal();
        if (connection != null &&  stream != null) {
          System.out.println("Broadcast started for: " + stream.getPublishedName());
          connection.setAttribute("streamStart", System.currentTimeMillis());
          connection.setAttribute("streamName", stream.getPublishedName());
        }
    }
	
    //Get list liveStrems
	public List<String> getLiveStreams() {

		Iterator<IClient> iter = scope.getClients().iterator();
        List<String> streams = new ArrayList<String>();

        THE_OUTER:while(iter.hasNext()) {

          IClient client = iter.next();
          Iterator<IConnection> cset = client.getConnections().iterator();

          THE_INNER:while(cset.hasNext()) {
            IConnection c = cset.next();
            if (c.hasAttribute("streamName")) {
              if (!c.isConnected()) {
                try {
                  c.close();
                  client.disconnect();
                }
                catch(Exception e) {
                  // Failure to close/disconnect.
                }
                continue THE_OUTER;
              }

              if (streams.contains(c.getAttribute("streamName").toString())) {
                continue THE_INNER;
              }

              streams.add(c.getAttribute("streamName").toString());
            }
          }
        }

		 return streams;

		}
