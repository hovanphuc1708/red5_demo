<%@ page import="com.red5pro.example.*, java.util.*, java.io.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
            <%@ page import="org.springframework.context.ApplicationContext,
                    com.red5pro.server.secondscreen.net.NetworkUtil,
                    org.springframework.web.context.WebApplicationContext,
                    com.red5pro.example.ExampleApplication,
                    java.util.List,
                    java.net.Inet4Address"%>

            <%

              String ip =  NetworkUtil.getLocalIpAddress();
              ApplicationContext appCtx = (ApplicationContext) application.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
              <!-- ??? -->
              ExampleApplication service = (ExampleApplication) appCtx.getBean("web.handler");

              List<String> streamNames = service.getLiveStreams();
              StringBuffer buffer = new StringBuffer();

              if(streamNames.size() == 0) {
                buffer.append("No streams found. Refresh if needed.");
              }
              else {
                buffer.append("<ul>\n");
                for (String streamName:streamNames) {
                  buffer.append("<li><a>" + streamName + " on " + ip + "</a></li>\n");
                }
                buffer.append("</ul>\n");
              }

             %>

            <!doctype html>
            <html>
            <body>
              <div>
                # Streams on Red5ProLive
                <%=buffer.toString()%>
              </div>
              <hr>
              <div>
                ## Start a broadcast session on your device to see it listed!
                <p>In the Settings dialog of the [Red5 Pro Application](https://github.com/red5pro) enter these values:</p>
                <table>
                  <tr>
                    <td>Server</td>
                    <td>**<%=ip%>**</td>
                  </tr>
                  <tr>
                    <td>App Name</td>
                    <td>**red5prolive**</td>
                  </tr>
                  <tr>
                    <td>Stream Name</td>
                    <td>**helloWorld**</td>
                  </tr>
                </table>
              </div>
            </body>
            </html>